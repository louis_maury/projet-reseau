# Projet reseau

[**Diaporama**](https://gitlab.com/louis_maury/projet-reseau/-/blob/master/CI_CD.pdf) 

Avant de commmencer à rejoindre le projet, vous pouvez lire les trois fichiers markdown de compte-rendu.

## Rejoindre le projet

Pour rejoindre le projet et faciliter le process de deploiement des différentes VMs, nous avons wrappé les commandes des différentes machines virtuelles sous Vagrant.

Une fois le projet cloné, vous pouvez vous déplacer dans le dossier vagrant :

`cd vagrant/` et faire la commande `vagrant up` afin de lancer les quatres VMs

Pour en savoir plus sur les actions effectuées par Vagrant, nous avons rédigé une [documentation complémentaire (Wiki)](https://gitlab.com/louis_maury/projet-reseau/-/wikis/Cr%C3%A9er-un-CI/sommaire)

## Se connecter au serveur Gitlab

### Créer son compte

Pour vous connecter, rendez-vous sur l'adresse suivante : `192.168.56.11` (le Gitlab Local) et définissez votre mot de passe.

Ensuite, connectez-vous avec vos credentials :

identifiant : `root`

mot de passe : `celui que vous avez défini`

### Créer son projet

Le serveur est initialié sans aucun projet. Vous pouvez créer un projet afin d'obtenir le token généré par Gitlab et qui sera utile pour votre Gitlab Runner.

## Se connecter au Registry

Sur le serveur Gitlab Local, il y a aussi un conteneur faisant tourner le Registry afin d'activer cette option sur Gitlab (cela nous permettra d'héberger des images en local).

Rendez-vous dans `Packages` > `Container Registry`. Vous pouvez y trouver 3 commandes, notamment celle permettant à d'autres machines de se connecter au registry.

Les trois commandes dans notre cas :

```shell
docker login gitlabServe.gitlab:5000
docker build -t gitlabServe.gitlab:5000/votre_nom/votre_projet .
docker push gitlabServe.gitlab:5000/votre_nom/votre_projet
```

Vous pouvez aller sur la VM `Dev` et vous connecter au Registry avec la commande : `docker login gitlabServe.gitlab:5000`

Un message devrait vous demander un identifiant et un mot de passe mais cela n'a pas d'importance pour la suite (l'identifiant et le mot de passe que vous voulez) :

```shell
sudo docker login gitlabServe.gitlab:5000
Username: root
Password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Félicitations, nous sommes connecté au Registry. Désormais, nous pouvons télécharger (pull) des images Docker et envoyer (push) des images (que nous avons contruites par exemple).

### Cas pratique

Nous allons améliorer une image. Créez un Dockerfile avec pour contenu :

```Dockerfile
FROM alpine:latest 
WORKDIR .
RUN apk add curl
```

Désormais, nous devons `build` notre image. Pour ce faire, effectuez la commande : `docker build -t gitlabServe.gitlab:5000/root/projet-de-root .`

Désormais, vous pouvez voir cette nouvelle image avec celle de alpine grâce à la commande : `docker images` :

```shell
REPOSITORY                                    TAG                 IMAGE ID            CREATED             SIZE
gitlabServe.gitlab:5000/root/projet-de-root   latest              c8e1a926ea1f        5 seconds ago       8.52MB
alpine                                        latest              e7d92cdc71fe        5 weeks ago         5.59MB
```

Nous pouvons désormais l'héberger sur notre Registry avec la commande : `docker push gitlabServe.gitlab:5000/root/projet-de-root:latest`

Vous pouvez retourner sur votre Gitlab Local dans `Packages` > `Container Registry` pour voir votre image !

## Créer un runner

Connectez-vous à la VM `Runner`, en SSH par exemple avec la commande `vagrant ssh Runner`.

Ensuite, vous pouvez créer votre premier runner avec la commande : `sudo gitlab-runner register`. Plusieurs informations vont vous être démandées.

- Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/): `http://gitlabServe.gitlab`
- Please enter the gitlab-ci token for this runner: (Pour trouver votre token, rendez-vous sur le serveur gitlab puis `settings` > `CI/CD` > `Runners Expand`)
- Please enter the gitlab-ci description for this runner: Ce que vous voulez
- Please enter the gitlab-ci tags for this runner (comma separated): Les tags qui vont déclencher ce runner en fonction du `.gitlab-ci.yml`
- Please enter the executor: docker-ssh, ssh, kubernetes, custom, docker, parallels, shell, virtualbox, docker+machine, docker-ssh+machine: Celui que vous voulez

Désormais, vous pouvez rafraîchir la page de votre Gitlab Local et voir apparaître le Runner avec une pastille verte. Votre runner est prêt !


