# Projet Infra - Compte rendu du groupe

## Projet

Pendant nos stages respectifs et via notre veille technologique, nous avons beaucoup entendu parler de déploiement continu et de livraison continue (CI/CD) sans avoir pu s'y pencher plus ainsi que de Docker.

Il nous est donc venu à l'idée de lier nos compétences afin de monter une infrastructure se rapprochant de celle que nous pouvons retrouver dans les entreprises avec un gitlab, un système de CI/CD, la mise en place d'un registry, la mise en place d'un serveur de développement et d'un serveur de production tout en automatisant la création de certains serveurs.


## Objectifs

### Objectifs personnels

- Comprendre et monter en compétences sur le CI/CD
- Comprendre et monter en compétences sur Docker
- Lier nos différentes compétences pour monter une infrastructure cohérente dans un réseau (Production, Développement, Gitlab, Runners)
- Mettre en pratique l'outil Vagrant


### Objectis du projet

- Installer une machine virtuelle avec Gitlab CE
- Installer une machine virtuelle avec des Runners
- Installer une machine virtuelle simulant la machine d'un développeur
- Installer une machine virtuelle simulant un serveur de production
- Activer le Registry sur Gitlab CE tournant sous Docker
- Créer une documentation permettant à n'importe qui de rejoindre le projet


## Avancées récentes

- Automatisation de l'installation des 4 VMs grâce à Vagrant (Gitlab CE, Gitlab-Runner, Développement, Production)
- Mise en place du Registry
- Création de différents Runners
