# Thibault FEUGERE - Compte rendu


## Objectifs

- Monter en compétences sur le CI/CD
- Continuer l'apprentissage de Docker
- Reproduire l'infrastructure vue en entreprise


## Avancées

- Installation de Gitlab CE
- Mise en place d'un CI/CD basique
- Configuration de Gitlab-Runner et création d'un Runner
- Activation du Registry

## Problèmes rencontrés

- Grosses difficultés sur l'activation du Registry à cause d'une mauvaise compréhension de la documentation officielle de Gitlab.
