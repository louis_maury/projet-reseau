# Louis MAURY - Compte rendu


## Objectifs

- Déconvrir le principe de CI/CD et du DevOps.
- Apprendre à utiliser Docker.

## Avancées

- Installation de Gitlab CE.
- Mise en place d'un CI/CD basique.
- Configuration de Gitlab-Runner et création d'un Runner.
- Activation du Registry.
- Simplification de la création des machines virtuels (VMs) avec Vagrant.

## Problèmes rencontrés

- Grosses difficultés sur l'activation du Registry à cause d'une mauvaise compréhension de la documentation officielle de Gitlab.
