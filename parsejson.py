import json, unittest

def symbol():
    with open("data.json", "r") as datafile:
        data = json.load(datafile)

        for i in range(10):
            coin = data[i]
            return (coin["symbol"])

class TestCase(unittest.TestCase):
    def test_sybmol(self):
        result = symbol()
        self.assertEqual(result, "BTC")

if __name__ == '__main__':
    # unittest.main()
    print("Go !")
